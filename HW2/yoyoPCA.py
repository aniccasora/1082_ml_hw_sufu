import cv2
from sklearn.decomposition import PCA


# retention_of_amount = 預設保留全部元素
def compression(img, retention_of_amount=None):

    # 原圖 w, h。
    w, h = img.shape[1::-1]

    # 一律轉為 gray。
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # 主成分轉換

    # 建立 PCA instance
    pca = PCA(retention_of_amount)
    # 將原圖丟進去訓練。
    img_lower_dim = pca.fit_transform(img)
    # 將降維的圖片再轉為原圖的大小。
    approximation = pca.inverse_transform(img_lower_dim)

    return approximation

