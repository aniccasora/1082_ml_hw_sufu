import cv2
import matplotlib.pyplot as plt
import yoyoPCA

if __name__ == "__main__":
    # 讀入圖片。
    origin_img = cv2.imread("img\\dog_and_its_best_host.png")
    # 圖片 w, h。
    w, h = origin_img.shape[1::-1]
    # 建立  figure。
    fig = plt.figure("忠臣狗狗與他的主人", figsize=(16, 8))
    # 2 by 3 的輸出 plot。
    columns = 3
    rows = 2

    # 決定圖片要縮多少至多少解析度的數列,取最小邊計算
    min_side = min(w, h)
    size_list = [int(min_side/2**i) for i in range(6)]

    # 共輸出 6 張圖片。
    for i in range(1, columns*rows+1):
        plt.subplot(rows, columns, i)
        plt.title("dog-"+" {} pixel".format(size_list[i-1]))
        # 做 PCA。
        after = yoyoPCA.compression(origin_img, retention_of_amount=size_list[i-1])
        # 秀在 此時的位置，matplot 預設 cmap 不是灰階，而是一個熱度圖。
        plt.imshow(after, cmap="gray")
    plt.show()

