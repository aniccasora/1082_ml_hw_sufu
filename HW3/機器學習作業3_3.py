# -*- coding: utf-8 -*-
import tensorflow as tf

if __name__ == "__main__" :

    # 取得 mnist 資料集
    mnist = tf.keras.datasets.mnist

    # 將 mnist 資料集劃分。訓練 && 測試， x 是圖片(28 by 28: 0~255)
    (x_train, y_train),(x_test, y_test) = mnist.load_data()

    # 將 x 內的pixel資料都降至 [0.0 ~ 1.0]，_f 後輟的代表被轉到 浮點數
    x_train_f, x_test_f = x_train / 255.0, x_test / 255.0


    y_train = tf.keras.utils.to_categorical(y_train, 10)
    y_test = tf.keras.utils.to_categorical(y_test, 10)

    #  keras 的文件有提到 卷積 的輸入會被定義為 (批次,row,col,channel)，批次可忽略。
    # 所以我們的 形狀就為。
    input_shape = (28,28,1)
    
    # 不用 relu，我要用 leakyRelu
    LeakReLu = tf.keras.layers.LeakyReLU(alpha = 0.2)

    # 開始串 model
    model = tf.keras.models.Sequential([
        # 第一層 卷積, 第一層要給輸入大小
        tf.keras.layers.Conv2D(filters=32,kernel_size=(3,3),activation = LeakReLu,input_shape = input_shape ),
         # 再來一次
        tf.keras.layers.Conv2D(filters=64,kernel_size=(3,3),activation = LeakReLu),
        # 第二層 池化
        tf.keras.layers.MaxPooling2D(pool_size=(2, 2)),
        # 靜化神經元
        tf.keras.layers.Dropout(0.25),
        # 將 卷積的輸出 2d -> 1d(鋪平)
        tf.keras.layers.Flatten(),
        # 加入全連接層 
        tf.keras.layers.Dense(units=128, activation=LeakReLu), 
        # 靜化神經元
        tf.keras.layers.Dropout(0.5),
        # 最後 縮到 10 個類別後 套 softmax ，讓其輸出是機率分布。
        tf.keras.layers.Dense(units=10, activation='softmax')
    ])
    # ===============================

    # 看看模型的樣子
    model.summary()

    # 為模型設定 損失函數
    loss_fn = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
    # 下面的損失函數 不能是 SparseCategoricalCrossentropy。
    
    # 編譯模型
    model.compile(optimizer=tf.keras.optimizers.Adadelta(),
                  loss=tf.keras.losses.categorical_crossentropy,
                  metrics=['accuracy'])

    # 預設是4 dim 訓練， ，但這邊只有 3維度
    print(x_train_f.shape)

    # 這邊做個 reshape
    x_train_f = x_train_f.reshape(x_train_f.shape[0],28,28,1)
    x_test_f = x_test_f.reshape(x_test_f.shape[0],28,28,1)
    print(x_train_f.shape)

    print("x_train_f.shape:",x_train_f.shape)
    print("y_train  .shape:",y_train.shape)
    
    # 訓練模型，先前訓練 500次但是，好像到 10就幾乎收斂了
    model.fit(x_train_f, y_train, batch_size =128, epochs=5000)

    # 驗證模型， verbose 是決定要不要印出進度條等詳細資訊。
    score = model.evaluate(x_test_f, y_test, verbose=2)

    print('Test loss:', score[0])
    print('Test accuracy:', score[1])
    print("=== end ===")

