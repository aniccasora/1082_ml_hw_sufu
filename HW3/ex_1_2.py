import tensorflow as tf
from tensorflow.keras.layers import Dense, Flatten, Conv2D
from tensorflow.keras import Model
import matplotlib.pyplot as plt
import numpy as np

if __name__ == "__main__" :
    print("tf.test.is_built_with_cuda() = ",tf.test.is_built_with_cuda())

    # 取得 mnist 資料集
    mnist = tf.keras.datasets.mnist

    # 將 mnist 資料集劃分。訓練 && 測試， x 是圖片(28 by 28: 0~255)
    (x_train, y_train),(x_test, y_test) = mnist.load_data()

    # 將 x 內的pixel資料都降至 [0.0 ~ 1.0]，_f 後輟的代表被轉到 浮點數
    x_train_f, x_test_f = x_train / 255.0, x_test / 255.0

    # 開始串 model
    model = tf.keras.models.Sequential([
        # 第一層 輸入層
        tf.keras.layers.Flatten(input_shape=(28, 28)),
        # 第二層 加入全連接層
        # @units: 正整數, 輸出的為度(有幾個神經元).
        # @activation: 設定 激勵函式，預設式 y(x) = x.
        tf.keras.layers.Dense(units=128, activation='relu'), # 有無隱藏層只差在這邊
        tf.keras.layers.Dense(10)
    ])

    # 把第一筆 丟進model進行預測，此時 model 尚未訓練。
    predictions = model(x_train_f[:1]).numpy()
    # 取得訓練結果，這邊是 NN網路的輸出，predictions.shape = (1,10)，會有負號。

    # 顯示 model 尚未訓練的預測結果。
    fig = plt.figure('first',figsize=(10,4))
    fig.canvas.set_window_title('尚未訓練的模型精準度 視覺化')
    plt.subplots_adjust(left=0.05, right=0.95, top=0.9, bottom=0.2,
                        wspace = 0.5)

    # 印出 mnist 的數字圖片
    plt.subplot(1, 3, 1) # 設定子圖位置
    plt.title('Mnist Data') # (121):
    plt.imshow(x_train[:1].reshape(28,28) ,cmap = 'gray')

    # 出分類出來的 class 圖表
    plt.subplot(1, 3, 2)# 設定子圖位置
    plt.title('Classification')
    class_idx = predictions[0].tolist() # 分類出來的 類別, 一開始會預測出負號。
    x = [i for i in range(10)]
    plt.xticks(x)
    plt.xlabel('predict class: ' + str(class_idx.index(max(class_idx)) + 1))
    y = predictions[0].tolist() # predictions: shape=(1,10)
    plt.bar(x, y)

    # 因為預測完的結果會有負號的 probability，套個 softmax 後再輸出機率值。
    predictions_softmaxied = tf.nn.softmax(predictions).numpy()
    # 出分類出來的 class 圖表，softmax 過後
    plt.subplot(1, 3, 3)  # 設定子圖位置
    plt.title('Softmax')
    x = [i for i in range(10)]
    plt.xticks(x) # 使 x 軸必定整數
    plt.xlabel('predict class: ' + str(class_idx.index(max(class_idx)) + 1))
    y = predictions_softmaxied[0].tolist()
    plt.bar(x, y)
    # plt.show()
    # ===============================

    # 為模型設定 損失函數
    loss_fn = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)

    # 編譯模型
    model.compile(optimizer='adam',
                  loss=loss_fn,
                  metrics=['accuracy'])

    # 訓練模型
    model.fit(x_train_f, y_train, epochs=5)

    # 驗證模型， verbose 是決定要不要印出進度條等詳細資訊。
    model.evaluate(x_test_f, y_test, verbose=2)

    # 幫練好的 model 最後套一個 softmax。
    probability_model = tf.keras.Sequential([
        model,
        tf.keras.layers.Softmax()
    ])

    # 顯示結果
    probability_model(x_test_f[:5])

    # 預測張圖片
    pred_result = probability_model.predict(x_test[:1]) # 取得圖片
    pred_result = pred_result.tolist()[0]
    # pred_result = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0]
    # 印出圖片
    fig_r = plt.figure()
    fig_r.canvas.set_window_title('模型預測結果')
    plt.imshow(x_test[0].reshape(28, 28), cmap='gray')
    class_type = pred_result.index(max(pred_result))  # 取得 第幾個 idx 的值最大 就是那一個 idx 。
    plt.xlabel("pred_result = " +  str(class_type))
    plt.show()
    print("=== end ===")

