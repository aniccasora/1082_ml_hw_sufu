import numpy as np
import csv
import matplotlib.pyplot as plt

if __name__ == "__main__":
    if False:
        file = open('compare_table.csv', encoding='utf8')
        csv_reader = csv.reader(file)

        # 不讀取第一列
        catogy =next(csv_reader)
        print(catogy)
        rows = []
        for row in csv_reader:
            # row[1:] # 去除掉第一個資料(迭代次數)
            rows.append(row[1:])

        # 將讀入的 資料轉 ndarray
        NA = np.asarray(rows)
        # 轉浮點型態
        NA = NA.astype('float64')

        # 無隱藏層的 Loss
        no_hidden_layer_Loss = NA[:, 0]

        # 無隱藏層的 Accuracy
        no_hidden_layer_Accuracy = NA[:, 1]

        # 有隱藏層的 Loss
        have_hidden_layer_Loss = NA[:, 2]

        # 有隱藏層的 Accuracy
        have_hidden_layer_Accuracy = NA[:, 3]

        # x 軸 迭代次數
        x = range(1,501) # [0, 501) = 0~500

        # pyplot 的 figure 的設定
        fig = plt.figure(figsize=(15,9))
        fig.canvas.set_window_title('比較表')
        plt.subplots_adjust(left=0.08, right=0.98, top=0.96, bottom=0.11,
                            wspace=0.29, hspace = 0.63)
        # ----------------------------------
        plt.subplot(2,2,1)
        plt.title('Loss compare')
        plt.plot(x, no_hidden_layer_Loss,label="No_hidden",color='b') # 繪製損失 無隱藏層
        plt.plot(x, have_hidden_layer_Loss,label="Hidden",color='g')  # 繪製損失 有隱藏層
        plt.xlabel("Epoch", fontsize=10, labelpad=15)
        plt.ylabel("loss", fontsize=10, labelpad=15)
        # 顯示出線條標記位置
        plt.legend(loc="best")
        # ----------------------------------
        plt.subplot(2, 2, 2)
        plt.title('Accuracy compare')
        plt.plot(x, no_hidden_layer_Accuracy, label="No_hidden", color='b')  # 精準度
        plt.plot(x, have_hidden_layer_Accuracy, label="Hidden", color='g')  # 精準度
        plt.xlabel("Epoch", fontsize=10, labelpad=15)
        plt.ylabel("Acc", fontsize=10, labelpad=15)
        # 顯示出線條標記位置
        plt.legend(loc="best")
        # ---------------------------------- 因為疊代 500 次有點太多，後面數值近乎不變
        max_of_epoch = 10  # 最大疊代
        # ---------------------------------- 所以將 視界調整至有顯著變化的區段。 約為 1~10
        plt.subplot(2, 2, 3)
        plt.title('Loss compare, epoch[1 ~ '+str(max_of_epoch)+']')
        plt.plot(x[:max_of_epoch], no_hidden_layer_Loss[:max_of_epoch], label="No_hidden", color='b')  # 繪製損失 無隱藏層
        plt.plot(x[:max_of_epoch], have_hidden_layer_Loss[:max_of_epoch], label="Hidden", color='g')  # 繪製損失 有隱藏層
        plt.xlabel("Epoch", fontsize=10, labelpad=15)
        plt.xticks(x[:max_of_epoch])  # 使 x 軸必定整數
        plt.ylabel("loss", fontsize=10, labelpad=15)
        # 顯示出線條標記位置
        plt.legend(loc="best")
        # ----------------------------------
        plt.subplot(2, 2, 4)
        plt.title('Accuracy compare, epoch[1 ~ '+str(max_of_epoch)+']')
        plt.plot(x[:max_of_epoch], no_hidden_layer_Accuracy[:max_of_epoch], label="No_hidden", color='b')  # 精準度
        plt.plot(x[:max_of_epoch], have_hidden_layer_Accuracy[:max_of_epoch], label="Hidden", color='g')  # 精準度
        plt.xlabel("Epoch", fontsize=10, labelpad=15)
        plt.xticks(x[:max_of_epoch]) # 使 x 軸必定整數
        plt.ylabel("Acc", fontsize=10, labelpad=15)
        # 顯示出線條標記位置
        plt.legend(loc="best")

        plt.show()

        file.close()

    # 分析 cnn 結果

    file = open('CNN 訓練結果.txt', encoding='utf8')

    loss = []
    acc = []
    # 略過開頭
    next(file) # 略過開頭
    next(file) # 略過開頭
    for line in file:
        elements = line.rstrip().split(' ')
        # loss, acc
        # print(elements[-4], elements[-1])
        if elements[0] != "Epoch":
            break
        loss.append(elements[-4])
        acc.append(elements[-1])


    loss = np.asarray(loss).astype('float32')
    acc = np.asarray(acc).astype('float32')

    x = range(1,loss.shape[0]+1)

    fig = plt.figure(figsize=(8, 4))
    fig.canvas.set_window_title('比較表')
    plt.subplots_adjust(left=0.1, right=0.99, top=0.93, bottom=0.16,
                        wspace=0.35, hspace=3)
    # ----------------------------------
    plt.subplot(3, 2, 1)
    plt.title('Loss')
    plt.plot(x, loss, label="Loss", color='r')  # 繪製損失 無隱藏層
    plt.xlabel("Epoch", fontsize=10, labelpad=0.1)
    plt.ylabel("loss", fontsize=10, labelpad=15)
    # 顯示出線條標記位置
    plt.legend(loc="best")
    # ----------------------------------
    plt.subplot(3, 2, 2)
    plt.title('Accuracy')
    plt.plot(x, acc, label="acc", color='b')  # 精準度
    plt.xlabel("Epoch", fontsize=10, labelpad=0)
    plt.ylabel("Acc", fontsize=10, labelpad=15)
    # 顯示出線條標記位置
    plt.legend(loc="best")
    # ----------------------------------
    max_of_epoch_1 = 100  # 最大疊代
    # ----------------------------------
    plt.subplot(3, 2, 3)
    plt.title('Loss, epoch[1 ~ '+str(max_of_epoch_1)+']')
    plt.plot(x[:max_of_epoch_1], loss[:max_of_epoch_1], label="Loss", color='r')  # 繪製損失 無隱藏層
    plt.xlabel("Epoch", fontsize=10, labelpad=15)
    plt.ylabel("loss", fontsize=10, labelpad=15)
    # 顯示出線條標記位置
    plt.legend(loc="best")

    # ----------------------------------
    plt.subplot(3, 2, 4)
    plt.title('Accuracy, epoch[1 ~ '+str(max_of_epoch_1)+']')
    plt.plot(x[:max_of_epoch_1], acc[:max_of_epoch_1], label="acc", color='b')  # 精準度
    plt.xlabel("Epoch", fontsize=10, labelpad=15)
    plt.ylabel("Acc", fontsize=10, labelpad=15)
    # 顯示出線條標記位置
    plt.legend(loc="best")
    # ----------------------------------
    max_of_epoch_2 = 1000  # 第二次最大疊代
    # ----------------------------------
    plt.subplot(3, 2, 5)
    plt.title('Loss, epoch['+ str(max_of_epoch_1)+' ~ ' + str(max_of_epoch_2) + ']')
    plt.plot(x[max_of_epoch_1:max_of_epoch_2], loss[max_of_epoch_1:max_of_epoch_2], label="Loss", color='r')  # 繪製損失 無隱藏層
    plt.xlabel("Epoch", fontsize=10, labelpad=15)
    plt.ylabel("loss", fontsize=10, labelpad=15)
    # 顯示出線條標記位置
    plt.legend(loc="best")

    # ----------------------------------
    plt.subplot(3, 2, 6)
    plt.title('Accuracy, epoch['+ str(max_of_epoch_1)+' ~ ' + str(max_of_epoch_2) + ']')
    plt.plot(x[max_of_epoch_1:max_of_epoch_2],acc[max_of_epoch_1:max_of_epoch_2], label="acc", color='b')  # 精準度
    plt.xlabel("Epoch", fontsize=10, labelpad=15)
    plt.ylabel("Acc", fontsize=10, labelpad=15)
    # 顯示出線條標記位置
    plt.legend(loc="best")

    plt.show()